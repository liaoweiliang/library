#pragma checksum "D:\books\Libiary\Views\Books\Create.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "426968ed22414422cf3ad2dc1b1e0a8531b05b71"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Books_Create), @"mvc.1.0.view", @"/Views/Books/Create.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Books/Create.cshtml", typeof(AspNetCore.Views_Books_Create))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\books\Libiary\Views\_ViewImports.cshtml"
using Libiary;

#line default
#line hidden
#line 2 "D:\books\Libiary\Views\_ViewImports.cshtml"
using Libiary.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"426968ed22414422cf3ad2dc1b1e0a8531b05b71", @"/Views/Books/Create.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1b96a05f2531a85542586f93e99b6da4daa11aad", @"/Views/_ViewImports.cshtml")]
    public class Views_Books_Create : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Libiary.Models.Books>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(29, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "D:\books\Libiary\Views\Books\Create.cshtml"
  
    ViewData["Title"] = "Create";

#line default
#line hidden
            BeginContext(73, 4, true);
            WriteLiteral("    ");
            EndContext();
            BeginContext(77, 2185, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d5e60259f82f4756809bf094da6cfb22", async() => {
                BeginContext(97, 43, true);
                WriteLiteral("\r\n        <legend>添加图书信息</legend>\r\n        ");
                EndContext();
                BeginContext(141, 37, false);
#line 8 "D:\books\Libiary\Views\Books\Create.cshtml"
   Write(Html.HiddenFor(model => model.BookId));

#line default
#line hidden
                EndContext();
                BeginContext(178, 227, true);
                WriteLiteral("\r\n        <div class=\"layui-form-item\">\r\n            <label class=\"layui-form-label\" style=\"width:120px\">图书名称</label>\r\n            <div class=\"layui-input\">\r\n                <div class=\"layui-input-block\">\r\n                    ");
                EndContext();
                BeginContext(406, 39, false);
#line 13 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.EditorFor(model => model.BookName));

#line default
#line hidden
                EndContext();
                BeginContext(445, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(468, 50, false);
#line 14 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.ValidationMessageFor(model => model.BookName));

#line default
#line hidden
                EndContext();
                BeginContext(518, 285, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label"" style=""width:120px"">作者</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(804, 37, false);
#line 22 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.EditorFor(model => model.Author));

#line default
#line hidden
                EndContext();
                BeginContext(841, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(864, 48, false);
#line 23 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.ValidationMessageFor(model => model.Author));

#line default
#line hidden
                EndContext();
                BeginContext(912, 286, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label"" style=""width:120px"">出版社</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(1199, 36, false);
#line 31 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.EditorFor(model => model.Press));

#line default
#line hidden
                EndContext();
                BeginContext(1235, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(1258, 47, false);
#line 32 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.ValidationMessageFor(model => model.Press));

#line default
#line hidden
                EndContext();
                BeginContext(1305, 287, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label"" style=""width:120px"">出版日期</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(1593, 38, false);
#line 40 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.EditorFor(model => model.PubDate));

#line default
#line hidden
                EndContext();
                BeginContext(1631, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(1654, 49, false);
#line 41 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.ValidationMessageFor(model => model.PubDate));

#line default
#line hidden
                EndContext();
                BeginContext(1703, 287, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label"" style=""width:120px"">图书简介</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(1991, 35, false);
#line 49 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.EditorFor(model => model.Jjie));

#line default
#line hidden
                EndContext();
                BeginContext(2026, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(2049, 46, false);
#line 50 "D:\books\Libiary\Views\Books\Create.cshtml"
               Write(Html.ValidationMessageFor(model => model.Jjie));

#line default
#line hidden
                EndContext();
                BeginContext(2095, 160, true);
                WriteLiteral("\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <p>\r\n            <button class=\"layui-btn\" type=\"submit\">立即提交</button>\r\n        </p>\r\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2262, 13, true);
            WriteLiteral("\r\n<div>\r\n    ");
            EndContext();
            BeginContext(2275, 30, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "ceb652a5943f4fed9bc1dd2233970a44", async() => {
                BeginContext(2297, 4, true);
                WriteLiteral("返回列表");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2305, 14, true);
            WriteLiteral("\r\n</div>\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Libiary.Models.Books> Html { get; private set; }
    }
}
#pragma warning restore 1591
