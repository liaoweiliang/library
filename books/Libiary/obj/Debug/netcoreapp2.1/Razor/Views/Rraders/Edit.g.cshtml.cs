#pragma checksum "D:\books\Libiary\Views\Rraders\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2d654746ab65b180e7346d706318318c05430159"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Rraders_Edit), @"mvc.1.0.view", @"/Views/Rraders/Edit.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Rraders/Edit.cshtml", typeof(AspNetCore.Views_Rraders_Edit))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\books\Libiary\Views\_ViewImports.cshtml"
using Libiary;

#line default
#line hidden
#line 2 "D:\books\Libiary\Views\_ViewImports.cshtml"
using Libiary.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2d654746ab65b180e7346d706318318c05430159", @"/Views/Rraders/Edit.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1b96a05f2531a85542586f93e99b6da4daa11aad", @"/Views/_ViewImports.cshtml")]
    public class Views_Rraders_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Libiary.Models.Rrader>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(30, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
  
    ViewData["Title"] = "Edit";

#line default
#line hidden
            BeginContext(72, 1616, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24893b11c3bc4ab9ae0cf12598f7ed5b", async() => {
                BeginContext(92, 33, true);
                WriteLiteral("\r\n    <legend>读者信息</legend>\r\n    ");
                EndContext();
                BeginContext(126, 39, false);
#line 8 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
Write(Html.HiddenFor(model => model.RraderId));

#line default
#line hidden
                EndContext();
                BeginContext(165, 204, true);
                WriteLiteral("\r\n    <div class=\"layui-form-item\">\r\n        <label class=\"layui-form-label\"style=\"width:120px\">姓名</label>\r\n        <div class=\"layui-input\">\r\n            <div class=\"layui-input-block\">\r\n                ");
                EndContext();
                BeginContext(370, 41, false);
#line 13 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
           Write(Html.EditorFor(model => model.RraderName));

#line default
#line hidden
                EndContext();
                BeginContext(411, 18, true);
                WriteLiteral("\r\n                ");
                EndContext();
                BeginContext(430, 52, false);
#line 14 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
           Write(Html.ValidationMessageFor(model => model.RraderName));

#line default
#line hidden
                EndContext();
                BeginContext(482, 252, true);
                WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"layui-form-item\">\r\n        <label class=\"layui-form-label\"style=\"width:120px\">年龄</label>\r\n        <div class=\"layui-input\">\r\n            <div class=\"layui-input-block\">\r\n                ");
                EndContext();
                BeginContext(735, 34, false);
#line 22 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
           Write(Html.EditorFor(model => model.Age));

#line default
#line hidden
                EndContext();
                BeginContext(769, 18, true);
                WriteLiteral("\r\n                ");
                EndContext();
                BeginContext(788, 45, false);
#line 23 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
           Write(Html.ValidationMessageFor(model => model.Age));

#line default
#line hidden
                EndContext();
                BeginContext(833, 252, true);
                WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"layui-form-item\">\r\n        <label class=\"layui-form-label\"style=\"width:120px\">住址</label>\r\n        <div class=\"layui-input\">\r\n            <div class=\"layui-input-block\">\r\n                ");
                EndContext();
                BeginContext(1086, 38, false);
#line 31 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
           Write(Html.EditorFor(model => model.Address));

#line default
#line hidden
                EndContext();
                BeginContext(1124, 18, true);
                WriteLiteral("\r\n                ");
                EndContext();
                BeginContext(1143, 49, false);
#line 32 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
           Write(Html.ValidationMessageFor(model => model.Address));

#line default
#line hidden
                EndContext();
                BeginContext(1192, 254, true);
                WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"layui-form-item\">\r\n        <label class=\"layui-form-label\"style=\"width:120px\">联系电话</label>\r\n        <div class=\"layui-input\">\r\n            <div class=\"layui-input-block\">\r\n                ");
                EndContext();
                BeginContext(1447, 36, false);
#line 40 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
           Write(Html.EditorFor(model => model.Phone));

#line default
#line hidden
                EndContext();
                BeginContext(1483, 18, true);
                WriteLiteral("\r\n                ");
                EndContext();
                BeginContext(1502, 47, false);
#line 41 "D:\books\Libiary\Views\Rraders\Edit.cshtml"
           Write(Html.ValidationMessageFor(model => model.Phone));

#line default
#line hidden
                EndContext();
                BeginContext(1549, 132, true);
                WriteLiteral("\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <p>\r\n        <button class=\"layui-btn\" type=\"submit\">立即提交</button>\r\n    </p>\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1688, 15, true);
            WriteLiteral("\r\n\r\n<div>\r\n    ");
            EndContext();
            BeginContext(1703, 30, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "9a28e5497db8455b8c55c5811bff686b", async() => {
                BeginContext(1725, 4, true);
                WriteLiteral("返回列表");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1733, 8, true);
            WriteLiteral("\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Libiary.Models.Rrader> Html { get; private set; }
    }
}
#pragma warning restore 1591
