#pragma checksum "D:\books\Libiary\Views\Borrows\Delete.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "68d48b473e25a826c590b0df16f25abdb89adb38"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Borrows_Delete), @"mvc.1.0.view", @"/Views/Borrows/Delete.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Borrows/Delete.cshtml", typeof(AspNetCore.Views_Borrows_Delete))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\books\Libiary\Views\_ViewImports.cshtml"
using Libiary;

#line default
#line hidden
#line 2 "D:\books\Libiary\Views\_ViewImports.cshtml"
using Libiary.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"68d48b473e25a826c590b0df16f25abdb89adb38", @"/Views/Borrows/Delete.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1b96a05f2531a85542586f93e99b6da4daa11aad", @"/Views/_ViewImports.cshtml")]
    public class Views_Borrows_Delete : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Libiary.Models.Borrow>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", "hidden", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(30, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
  
    ViewData["Title"] = "Delete";

#line default
#line hidden
            BeginContext(74, 11, true);
            WriteLiteral("<div>\r\n    ");
            EndContext();
            BeginContext(85, 2876, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b4b1ae388a774742b6651fa3534da22b", async() => {
                BeginContext(105, 43, true);
                WriteLiteral("\r\n        <legend>图书借还信息</legend>\r\n        ");
                EndContext();
                BeginContext(149, 39, false);
#line 9 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
   Write(Html.HiddenFor(model => model.BorrowId));

#line default
#line hidden
                EndContext();
                BeginContext(188, 226, true);
                WriteLiteral("\r\n        <div class=\"layui-form-item\">\r\n            <label class=\"layui-form-label\"style=\"width:120px\">图书名称</label>\r\n            <div class=\"layui-input\">\r\n                <div class=\"layui-input-block\">\r\n                    ");
                EndContext();
                BeginContext(415, 44, false);
#line 14 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayNameFor(model => model.BookName));

#line default
#line hidden
                EndContext();
                BeginContext(459, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(482, 40, false);
#line 15 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayFor(model => model.BookName));

#line default
#line hidden
                EndContext();
                BeginContext(522, 286, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label""style=""width:120px"">读者名字</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(809, 46, false);
#line 23 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayNameFor(model => model.ReaderName));

#line default
#line hidden
                EndContext();
                BeginContext(855, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(878, 42, false);
#line 24 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayFor(model => model.ReaderName));

#line default
#line hidden
                EndContext();
                BeginContext(920, 286, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label""style=""width:120px"">借阅日期</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(1207, 46, false);
#line 32 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayNameFor(model => model.BorrowDate));

#line default
#line hidden
                EndContext();
                BeginContext(1253, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(1276, 42, false);
#line 33 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayFor(model => model.BorrowDate));

#line default
#line hidden
                EndContext();
                BeginContext(1318, 286, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label""style=""width:120px"">借阅天数</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(1605, 40, false);
#line 41 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayNameFor(model => model.Days));

#line default
#line hidden
                EndContext();
                BeginContext(1645, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(1668, 36, false);
#line 42 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayFor(model => model.Days));

#line default
#line hidden
                EndContext();
                BeginContext(1704, 286, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label""style=""width:120px"">归还日期</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(1991, 44, false);
#line 50 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayNameFor(model => model.GuihDate));

#line default
#line hidden
                EndContext();
                BeginContext(2035, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(2058, 40, false);
#line 51 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayFor(model => model.GuihDate));

#line default
#line hidden
                EndContext();
                BeginContext(2098, 284, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label""style=""width:120px"">作者</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(2383, 40, false);
#line 59 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayNameFor(model => model.Book));

#line default
#line hidden
                EndContext();
                BeginContext(2423, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(2446, 43, false);
#line 60 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayFor(model => model.Book.Author));

#line default
#line hidden
                EndContext();
                BeginContext(2489, 286, true);
                WriteLiteral(@"
                </div>
            </div>
        </div>
        <div class=""layui-form-item"">
            <label class=""layui-form-label""style=""width:120px"">读者编号</label>
            <div class=""layui-input"">
                <div class=""layui-input-block"">
                    ");
                EndContext();
                BeginContext(2776, 42, false);
#line 68 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayNameFor(model => model.Reader));

#line default
#line hidden
                EndContext();
                BeginContext(2818, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
                BeginContext(2841, 47, false);
#line 69 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
               Write(Html.DisplayFor(model => model.Reader.RraderId));

#line default
#line hidden
                EndContext();
                BeginContext(2888, 66, true);
                WriteLiteral("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2961, 12, true);
            WriteLiteral("\r\n    \r\n    ");
            EndContext();
            BeginContext(2973, 227, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a3f3bca20d41486888e2546ca7e75da8", async() => {
                BeginContext(2999, 10, true);
                WriteLiteral("\r\n        ");
                EndContext();
                BeginContext(3009, 42, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "851f36d140874fa28a00343fa699b588", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.InputTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.InputTypeName = (string)__tagHelperAttribute_1.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
#line 76 "D:\books\Libiary\Views\Borrows\Delete.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For = ModelExpressionProvider.CreateModelExpression(ViewData, __model => __model.BorrowId);

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-for", __Microsoft_AspNetCore_Mvc_TagHelpers_InputTagHelper.For, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3051, 106, true);
                WriteLiteral("\r\n        <p>\r\n            <button class=\"layui-btn\" type=\"submit\">立即提交</button>\r\n        </p> |\r\n        ");
                EndContext();
                BeginContext(3157, 30, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "aafe3bd367d34034b700be71065f5ad8", async() => {
                    BeginContext(3179, 4, true);
                    WriteLiteral("返回列表");
                    EndContext();
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(3187, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3200, 10, true);
            WriteLiteral("\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Libiary.Models.Borrow> Html { get; private set; }
    }
}
#pragma warning restore 1591
