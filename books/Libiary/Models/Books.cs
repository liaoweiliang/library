﻿using System;
using System.Collections.Generic;

namespace Libiary.Models
{
    public partial class Books
    {
        public Books()
        {
            Borrow = new HashSet<Borrow>();
        }

        public int BookId { get; set; }
        public string BookName { get; set; }
        public string Author { get; set; }
        public string Press { get; set; }
        public DateTime PubDate { get; set; }
        public string Jjie { get; set; }

        public ICollection<Borrow> Borrow { get; set; }
    }
}
