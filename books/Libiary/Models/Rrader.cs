﻿using System;
using System.Collections.Generic;

namespace Libiary.Models
{
    public partial class Rrader
    {
        public Rrader()
        {
            Borrow = new HashSet<Borrow>();
        }

        public int RraderId { get; set; }
        public string RraderName { get; set; }
        public int? Age { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public ICollection<Borrow> Borrow { get; set; }
    }
}
