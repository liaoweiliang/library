﻿using System;
using System.Collections.Generic;

namespace Libiary.Models
{
    public partial class User
    {
        public int UserId { get; set; }
        public int UserTypeId { get; set; }
        public string UserName { get; set; }
        public string Pwd { get; set; }

        public UserType UserType { get; set; }
    }
}
