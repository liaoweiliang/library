﻿using System;
using System.Collections.Generic;

namespace Libiary.Models
{
    public partial class Borrow
    {
        public int BorrowId { get; set; }
        public int BookId { get; set; }
        public string BookName { get; set; }
        public int ReaderId { get; set; }
        public string ReaderName { get; set; }
        public DateTime BorrowDate { get; set; }
        public string Days { get; set; }
        public DateTime GuihDate { get; set; }

        public Books Book { get; set; }
        public Rrader Reader { get; set; }
    }
}
