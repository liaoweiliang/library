﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Libiary.Models;

namespace Libiary.Controllers
{
    public class RradersController : Controller
    {
        private readonly BookDBContext _context;

        public RradersController(BookDBContext context)
        {
            _context = context;
        }

        // GET: Rraders
        public async Task<IActionResult> Index()
        {
            return View(await _context.Rrader.ToListAsync());
        }
        [HttpPost]
        public ActionResult Index(string name)
        {
            BookDBContext db = new BookDBContext();
            var us = db.Rrader.Where(p => p.RraderName.Contains(name)).ToList();
            return View(us);
        }
        // GET: Rraders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rrader = await _context.Rrader
                .FirstOrDefaultAsync(m => m.RraderId == id);
            if (rrader == null)
            {
                return NotFound();
            }

            return View(rrader);
        }

        // GET: Rraders/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Rraders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RraderId,RraderName,Age,Address,Phone")] Rrader rrader)
        {
            
                _context.Add(rrader);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
          
        }

        // GET: Rraders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rrader = await _context.Rrader.FindAsync(id);
            if (rrader == null)
            {
                return NotFound();
            }
            return View(rrader);
        }

        // POST: Rraders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RraderId,RraderName,Age,Address,Phone")] Rrader rrader)
        {
            if (id != rrader.RraderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(rrader);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RraderExists(rrader.RraderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(rrader);
        }

        // GET: Rraders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rrader = await _context.Rrader
                .FirstOrDefaultAsync(m => m.RraderId == id);
            if (rrader == null)
            {
                return NotFound();
            }

            return View(rrader);
        }

        // POST: Rraders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rrader = await _context.Rrader.FindAsync(id);
            _context.Rrader.Remove(rrader);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RraderExists(int id)
        {
            return _context.Rrader.Any(e => e.RraderId == id);
        }
    }
}
