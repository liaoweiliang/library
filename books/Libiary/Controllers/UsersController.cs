﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Libiary.Models;

namespace Libiary.Controllers
{
    public class UsersController : Controller
    {
        private readonly BookDBContext _context;

        public UsersController(BookDBContext context)
        {
            _context = context;
        }
        
        //登录
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {

            using (BookDBContext db = new BookDBContext())
            {
                int a = db.User.Where(p => p.UserName == user.UserName && p.Pwd == user.Pwd).Count();
                if (a == 0)
                {
                    ModelState.AddModelError("", "账号或密码错误！");
                    return View();
                }
                else
                {
                    login.name = db.User.Single(p => p.UserName == user.UserName && p.Pwd == user.Pwd);
                    login.name.UserType = db.UserType.Single(p => p.UserTypeId == login.name.UserTypeId);
                    //login.name.UserName = db.User.SingleOrDefault(p => p.UserName == user.UserName && p.Pwd == user.Pwd).UserName;
                    return RedirectToAction("Index", "Users",new { id=login.name.UserId });
                }
            }
        }
        public async Task<IActionResult> Index()
        {
            var bookDBContext = _context.User.Include(u => u.UserType);
            return View(await bookDBContext.ToListAsync());
        }
        [HttpPost]
        public ActionResult Index(string name)
        {
            BookDBContext db = new BookDBContext();
            var us = db.User.Where(p => p.UserName.Contains(name)).ToList();
            return View(us);
        }
        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .Include(u => u.UserType)
                .FirstOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            ViewData["UserTypeId"] = new SelectList(_context.UserType, "UserTypeId", "UserTypeName");
            return View();
        }
        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,UserTypeId,UserName,Pwd")] User user)
        {
            _context.Add(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            ViewData["UserTypeId"] = new SelectList(_context.UserType, "UserTypeId", "UserTypeName", user.UserTypeId);
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UserId,UserTypeId,UserName,Pwd")] User user)
        {
            if (id != user.UserId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.UserId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserTypeId"] = new SelectList(_context.UserType, "UserTypeId", "UserTypeName", user.UserTypeId);
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .Include(u => u.UserType)
                .FirstOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _context.User.FindAsync(id);
            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.UserId == id);
        }
    }
}
